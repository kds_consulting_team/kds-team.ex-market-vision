Enter your username and password to retrieve all data from all projects in Market Vision. 
The data is extracted incrementally, so only data that has been changed since the last run is fetched. 
The component has backfill implemented, meaning that if not all data could be fetched in the first run due to large amounts of data, it will be fetched in the following runs.