from keboola.http_client import HttpClient

BASE_URL = "https://reporting.marketvision-spring.com/api"

ENDPOINT_PROJECTS = "v1/project"
ENDPOINT_FORM = "form"
ENDPOINT_QUESTION = "question"
ENDPOINT_CATEGORY = "category"
ENDPOINT_FIELDSET = "fieldset"


class MarketVisionClientException(Exception):
    pass


class MarketVisionClient(HttpClient):
    def __init__(self):
        super().__init__(BASE_URL)

    def login(self, username, password):
        data = {
            "grant_type": "password",
            "username": username,
            "password": password
        }
        access_token = self.post(endpoint_path="oauth2/token", data=data).get("access_token")
        if not access_token:
            raise MarketVisionClientException("Failed to generate api token, please check username and password")
        self.update_auth_header({"Authorization": f"Bearer {access_token}"}, overwrite=True)

    def get_projects(self):
        project_data = self.get(endpoint_path=ENDPOINT_PROJECTS)
        if "projects" not in project_data:
            raise MarketVisionClientException(f"Failed to get project info, API returned {project_data}")
        return project_data["projects"]

    def get_project_forms(self, project_id):
        endpoint = "/".join([ENDPOINT_PROJECTS, project_id, ENDPOINT_FORM])
        form_data = self.get(endpoint_path=endpoint)
        if "form" not in form_data:
            raise MarketVisionClientException(
                f"Failed to get form info for project id {project_id}, API returned {form_data}")
        return form_data["form"]

    def get_form_detail(self, project_id, form_id):
        endpoint = "/".join([ENDPOINT_PROJECTS, project_id, ENDPOINT_FORM, form_id])
        form_detail = self.get(endpoint_path=endpoint)
        return form_detail

    def get_project_questions(self, project_id):
        endpoint = "/".join([ENDPOINT_PROJECTS, project_id, ENDPOINT_FORM, ENDPOINT_QUESTION])
        questions = self.get(endpoint_path=endpoint)
        if "questions" not in questions:
            raise MarketVisionClientException(
                f"Failed to questions for project id {project_id}, API returned {questions}")
        return questions["questions"]

    def get_project_categories(self, project_id):
        endpoint = "/".join([ENDPOINT_PROJECTS, project_id, ENDPOINT_FORM, ENDPOINT_CATEGORY])
        categories = self.get(endpoint_path=endpoint)
        if "categories" not in categories:
            raise MarketVisionClientException(
                f"Failed to categories for project id {project_id}, API returned {categories}")
        return categories["categories"]

    def get_project_fieldsets(self, project_id):
        endpoint = "/".join([ENDPOINT_PROJECTS, project_id, ENDPOINT_FORM, ENDPOINT_FIELDSET])
        fieldsets = self.get(endpoint_path=endpoint)
        if "fieldsets" not in fieldsets:
            raise MarketVisionClientException(
                f"Failed to fieldsets for project id {project_id}, API returned {fieldsets}")
        return fieldsets["fieldsets"]
