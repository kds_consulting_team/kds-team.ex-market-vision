import csv
import logging
from timeit import default_timer as timer
from datetime import datetime
from requests import HTTPError
from market_vision import MarketVisionClient, MarketVisionClientException
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

KEY_USERNAME = "username"
KEY_PASSWORD = '#password'

KEY_STATE_PROJECTS_FETCHED = "projects_fetched"
KEY_STATE_LAST_RUN = "last_run"

MAX_RUNTIME = 3600

REQUIRED_PARAMETERS = [KEY_USERNAME, KEY_PASSWORD]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__()

    def run(self):
        start = timer()
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters
        state = self.get_state_file()

        username = params.get(KEY_USERNAME)
        password = params.get(KEY_PASSWORD)
        client = MarketVisionClient()
        try:
            client.login(username, password)
        except HTTPError as http_error:
            raise UserException(
                "Failed to login to market vision, please check your username and password") from http_error
        try:
            projects = client.get_projects()
        except HTTPError as http_error:
            raise UserException("Failed to get projects from MarketVision, "
                                "please check your username and password") from http_error

        state_projects_fetched = state.get(KEY_STATE_PROJECTS_FETCHED)
        last_run_str = state.get(KEY_STATE_LAST_RUN, "1990-01-01")
        last_run = datetime.strptime(last_run_str, '%Y-%m-%d')

        project_table = self.create_out_table_definition("projects.csv", incremental=True,
                                                         primary_key=["id"])
        self.write_to_table(project_table, projects)

        if not state_projects_fetched:
            state_projects_fetched = {}

        fetched_projects = {}

        project_form_detail = []
        project_form_detail_index = []
        project_form_detail_questions = []

        project_questions = []

        project_categories = []

        project_fieldsets = []

        for project in projects:
            project_id = str(project["id"])
            form_details, form_indexes, form_questions, form_ids = self.get_project_form_details(client,
                                                                                                 project_id,
                                                                                                 last_run,
                                                                                                 state_projects_fetched)
            fetched_projects[project_id] = form_ids

            project_form_detail.extend(form_details)
            project_form_detail_index.extend(form_indexes)
            project_form_detail_questions.extend(form_questions)

            questions = self.get_project_questions(client, project)
            project_questions.extend(questions)

            categories = self.get_project_categories(client, project)
            project_categories.extend(categories)

            fieldsets = self.get_project_fieldsets(client, project)
            project_fieldsets.extend(fieldsets)

            elapsed_time = timer() - start
            if elapsed_time > MAX_RUNTIME * 0.7:
                # stop if there is only 30% of time left
                logging.info(
                    "MAX RUNTIME of component has almost been reached, saving data. "
                    "Rerun the component to let backfill fetch all of your data")
                break

        logging.info('Saving all project form details')

        if project_form_detail:
            project_form_detail_table = self.create_out_table_definition("project_form_detail.csv", incremental=True,
                                                                         primary_key=["form_id", "project_id"])
            self.write_to_table(project_form_detail_table, project_form_detail)

        if project_form_detail_index:
            project_form_detail_index_table = self.create_out_table_definition("project_form_detail_index.csv",
                                                                               incremental=True,
                                                                               primary_key=["project_id", "form_id",
                                                                                            "index_category_id"])
            self.write_to_table(project_form_detail_index_table, project_form_detail_index)

        if project_form_detail_questions:
            project_form_detail_questions_table = self.create_out_table_definition("project_form_detail_questions.csv",
                                                                                   incremental=True,
                                                                                   primary_key=["project_id", "form_id",
                                                                                                "question_id"])
            self.write_to_table(project_form_detail_questions_table, project_form_detail_questions)

        logging.info('Saving all project questions')
        if project_questions:
            project_form_questions_table = self.create_out_table_definition("project_form_questions.csv",
                                                                            incremental=True,
                                                                            primary_key=["project_id", "question_id",
                                                                                         "fieldset_id",
                                                                                         "index_category_id",
                                                                                         "index_supercategory_id",
                                                                                         "option_id"])
            self.write_to_table(project_form_questions_table, project_questions)

        logging.info('Saving all project categories')
        if project_categories:
            project_form_categories_table = self.create_out_table_definition("project_form_categories.csv",
                                                                             incremental=True,
                                                                             primary_key=["project_id", "category_id"])
            self.write_to_table(project_form_categories_table, project_categories)

        logging.info('Saving all project fieldsets')
        if project_fieldsets:
            project_form_fieldsets_table = self.create_out_table_definition("project_form_fieldsets.csv",
                                                                            incremental=True,
                                                                            primary_key=["project_id", "fieldset_id"])
            self.write_to_table(project_form_fieldsets_table, project_fieldsets)

        current_date = datetime.today().strftime('%Y-%m-%d')
        new_state = {
            "last_run": current_date,
            "projects_fetched": fetched_projects
        }
        self.write_state_file(new_state)

    @staticmethod
    def _infer_fieldnames_from_data(data):
        if len(data) > 1:
            keys = data[0].keys()
            return list(keys)
        else:
            return []

    def write_to_table(self, table, data):
        fieldnames = self._infer_fieldnames_from_data(data)
        table.columns = fieldnames
        with open(table.full_path, 'w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=table.columns)
            writer.writerows(data)
        self.write_manifest(table)
        pass

    def get_project_form_details(self, client, project_id, last_run, state_projects_fetched):
        logging.info(f'Getting details for project ID {project_id}')
        project_forms = client.get_project_forms(project_id)
        project_form_detail = []
        project_form_detail_index = []
        project_form_detail_questions = []
        project_ids_fetched = state_projects_fetched.get(project_id, [])

        for i, form in enumerate(project_forms):
            form_id = str(form.get("id", ""))

            form_last_saved = self.get_form_last_run(form)
            if form_last_saved > last_run or form_id not in project_ids_fetched:
                self.log_progress(i, project_forms, project_id)
                try:
                    form_details = client.get_form_detail(project_id, form_id)
                    project_form_detail.extend(self.parse_form_details(form_details, project_id))
                    project_form_detail_index.extend(self.parse_form_detail_indexes(form_details, project_id, form_id))
                    project_form_detail_questions.extend(
                        self.parse_form_detail_questions(form_details, project_id, form_id))
                except (MarketVisionClientException, HTTPError):
                    logging.warning(
                        f"WARNING : could not fetch details for form ID {form['id']} in project ID {project_id} ")
                project_ids_fetched.append(form_id)

        return project_form_detail, project_form_detail_index, project_form_detail_questions, project_ids_fetched

    @staticmethod
    def parse_form_details(form_details, project_id):
        form_id = str(form_details.get("id", ""))
        status = str(form_details.get("status", ""))
        last_saved = str(form_details.get("last_saved", ""))
        return [{"form_id": form_id, "project_id": project_id, "status": status, "last_saved": last_saved}]

    @staticmethod
    def get_form_last_run(form):
        if isinstance(form['last_saved'], type(None)):
            return datetime.strptime('2000-01-01', '%Y-%m-%d')
        else:
            return datetime.strptime(form['last_saved'][:10], '%Y-%m-%d')

    @staticmethod
    def parse_form_detail_indexes(form_details, project_id, form_id):
        project_form_detail_index = []
        for form_indexes in form_details['index']:
            for form_index in form_indexes:
                project_form_detail_index_tmp = {
                    'project_id': str(project_id),
                    'form_id': form_id,
                    'index_category_id': str(form_index.get("category_id")),
                    'index_points': str(form_index.get("index_points")),
                    'index_max_points': str(form_index.get("index_max_points")),
                    'index': str(form_index.get("index"))
                }
                project_form_detail_index.append(project_form_detail_index_tmp)
        return project_form_detail_index

    @staticmethod
    def parse_form_detail_questions(form_details, project_id, form_id):
        project_form_detail_questions = []
        for question in form_details['questions']:
            project_form_detail_questions_tmp = {
                'project_id': str(project_id),
                'form_id': str(form_id),
                'question_id': str(question.get("id")),
                'question_option': str(question.get("option_id", "")),
                'question_value': str(question.get("value", "")),
            }

            project_form_detail_questions.append(project_form_detail_questions_tmp)
        return project_form_detail_questions

    def get_project_questions(self, client, project):
        project_id = str(project['id'])
        logging.info(f'Getting questions for project ID {project_id}')

        questions = client.get_project_questions(project_id)
        project_questions = []
        for question in questions:
            question_data = {
                'project_id': project_id,
                'question_id': question.get("id", ""),
                'name': question.get("name", ""),
                'type': question.get("type", ""),
                'fieldset_id': question.get("fieldset_id", ""),
                'text_type': question.get("text_type", ""),
                'multiple_response_set_name': question.get("multiple_response_set_name", ""),
                'index_category_id': question.get("index_category_id", ""),
                'index_supercategory_id': question.get("index_supercategory_id", ""),
                'variable': question.get("variable", ""),
                'attachment_type': question.get("attachment_type", ""),
            }
            if question.get("type") == 'single_response':
                parsed_question = self.get_multiple_choice_questions(question, question_data)
            else:
                parsed_question = self.get_open_question(question_data)
            project_questions.extend(parsed_question)
        return project_questions

    @staticmethod
    def get_multiple_choice_questions(question, question_data):
        questions = []
        for option in question.get('options'):
            current_question = question_data.copy()
            current_question["option_id"] = option.get("option_id")
            current_question["option_value"] = option.get("value")
            questions.append(current_question)

        return questions

    @staticmethod
    def get_open_question(question_data):
        question_data["option_id"] = ""
        question_data["option_value"] = ""
        return [question_data]

    @staticmethod
    def get_project_categories(client, project):
        project_id = str(project['id'])
        logging.info(f'Getting categories for project ID {project_id}')
        project_categories = client.get_project_categories(project_id=project_id)
        parsed_categories = []
        for category in project_categories:
            parsed_category = {
                'project_id': project_id,
                'category_id': category.get("id"),
                'name': category.get("name"),
                'type': category.get("type"),
                'default_index': category.get("default_index", "")
            }
            parsed_categories.append(parsed_category)
        return parsed_categories

    @staticmethod
    def get_project_fieldsets(client, project):
        project_id = str(project['id'])
        logging.info(f'Getting fieldsets for project ID {project_id}')
        project_fieldsets = client.get_project_fieldsets(project_id=project_id)
        parsed_fieldsets = []
        for fieldset in project_fieldsets:
            parsed_fieldset = {
                'project_id': project_id,
                'fieldset_id': fieldset.get("id"),
                'name': fieldset.get("name")
            }
            parsed_fieldsets.append(parsed_fieldset)
        return parsed_fieldsets

    @staticmethod
    def log_progress(i, project_forms, project_id, frequency=50):
        if i % frequency == 0:
            logging.info(
                f"Fetched details of {str(i)} out of {str(len(project_forms))} forms for project ID {project_id}")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
